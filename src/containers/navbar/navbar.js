import React from "react";
import "./navbar.css";


function Navbar() {
    return(
    <div>
      <header className="main-navbar">
        <span>
            <a href="#about" className="entry" style={{color: '#dd4a68'}}>About</a>
        </span>
        <span>
            <a href="#skills" className="entry">Skills</a>
        </span>
        <span>
            <a href="#education" className="entry">Education</a>
        </span>
        <span>
            <a href="#experience" className="entry">Experience</a>
        </span>
      </header>
    </div>
    );

}

export default Navbar;