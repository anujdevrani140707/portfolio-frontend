import React from "react";
import "./headers.css";


const Header = ( data ) => {
    return(
    <div>
      <header className="header">
        <div className="greeting">
          Hello I'm &nbsp;
              <span className="typewriter">
                { data.props.first_name + ' ' + data.props.last_name }
              </span>
        </div>
      </header>
    </div>
    );

}

export default Header;