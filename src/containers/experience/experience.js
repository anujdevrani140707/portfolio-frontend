import React from "react";
import "./experience.css";
import ExperienceComponent from '../../components/experienceComponent/experienceComponent'

const Experience = (data) => {
  const experience = data.props
  return (
	<div className="experienceSection" id="experience">
        <span className="experienceheader">
                EXPERIENCE
        </span>
        <div>
          {
          experience.map(experience => {
            return <ExperienceComponent key={experience.id} props={experience} />
          })
        }
        </div>
	</div>
  );
}

export default Experience;