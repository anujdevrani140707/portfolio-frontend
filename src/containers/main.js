import React, { Component } from "react";
import axios from 'axios'
import Header from '../containers/headers/headers'
import Navbar from '../containers/navbar/navbar'
import About from '../containers/about/about'
import Skills from '../containers/skills/skills'
import Education from '../containers/education/education'
import Experience from '../containers/experience/experience'
import Comment from '../containers/comment/comment'

export default class Main extends Component {
    state = {
        portfoliodata: null
    }

    componentDidMount(){
        axios.get(process.env.REACT_APP_BASE_API_URL + 'getportfoliodata/')
        .then(res => {
            const portfoliodata = res.data;
            console.log(portfoliodata);
            this.setState({ portfoliodata })
        })
    }
    render() {
        const portData = this.state.portfoliodata;
        if (!portData){
            return null;
        }
        return(
            <div>
                <Header props={ portData } />
                <Navbar />
                <About props={ portData.starter_description }/>
                <Skills props={ portData.user_skills } />
                <Education props={ portData.user_education } />
                <Experience props={ portData.user_experience } />
                <Comment />
            </div>
        );
    }
}