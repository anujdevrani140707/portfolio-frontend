import React from "react";
import "./skills.css";


const Skills = (data) => {
    const skills = data.props;

    return(
        <div className="skillsSection" id="skills">
            {
                skills.map(skill => {
                return <span className="skillName" key={skill.name}>{skill.name}</span>
                })
            }
        </div>
    );

}

export default Skills;