import React, { Component } from "react";
import axios from 'axios'
import './comment.css'

export default class Comment extends Component {
    state = {
        comment: null,
        pastComments: []
    }

    fetchComments(){
        axios.get(process.env.REACT_APP_BASE_API_URL + 'comment/')
        .then(res => {
            const comment_data = res.data.results;
            this.setState({ pastComments: comment_data })
            console.log(this.state)
        })
    }
    componentDidMount(){
        this.fetchComments()
    }
    handleSubmit = event => {
        event.preventDefault();
        console.log(this.state)
        
        axios.post(process.env.REACT_APP_BASE_API_URL + 'comment/', { "comment": this.state.comment},
        ).then(this.fetchComments());
      }

    handleChange = event =>{
        this.setState({ comment: event.target.value});
      }

    render() {
        const pastComments = this.state.pastComments;
        return(
            <div className="CommentSection">

                    <span className="discussheader">Discuss</span>

                <div className="commentBox">
                    <form onSubmit={this.handleSubmit}>
                        <input
                        id="comment"
                        name="Comment"
                        type="text"
                        onChange= {this.handleChange}
                        required
                        />
                        <button>Send</button>
                    </form>
                </div>
                <div>
                    <div className="pastComments">
                        {pastComments ? pastComments.map(com => {
                            return <div key={com.id}>
                                {com.comment}
                                </div>
                        }): ''}
                    </div>
                </div>
            </div>
        );
    }
}