import React from "react";
import "./education.css";
import EducationComponent from '../../components/educationComponent/educationComponent'


const Education = (data) => {
    const educationData = data.props;
    console.log(educationData)

    return(
        <div className="educationSection" id="education">
            <span className="educationheader">
                EDUCATION
            </span>
            {educationData.map(education => {
                return <EducationComponent key={education.institute_name} props={education}/>
            })}

        </div>
    );

}

export default Education;