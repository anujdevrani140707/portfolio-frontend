import React from "react";
import "./educationComponent.css";

const EducationComponent = (data) => {
  const education = data.props
  return (
	<div className="educationComponent">
		<div className="educationCard">
			<table>
				<tbody>
					<tr>
						<td>
							<div className="hatIcon">
								<img alt="hat" src={require("../../assets/icons/graduation-hat.svg")}></img>
							</div>
						</td>
						<td>
							<div className="container">
								<div>
									{education.start_date + ' - ' + education.end_date} 
								</div>
								<div style={{fontFamily: 'Agustina Regular', fontWeight: 'bold', fontSize:'20pt', paddingLeft:'5px'}}>
									{education.institute_name}
								</div>
								<div>
									{education.degree_name}
								</div>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

	</div>
  );
}

export default EducationComponent;