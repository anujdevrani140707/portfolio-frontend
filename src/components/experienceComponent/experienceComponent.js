import React from "react";
import './experienceComponent.css';

const ExperienceComponent = (data) => {
  const experience = data.props
  return (
	<div className="experienceComponent">
        <div>
            {experience.start_date + ' - ' + (experience.end_date===null ? 'Present': experience.end_date) } 
        </div>
        <div>
            <div>
                {experience.company_name}
            </div>
            <div>
                {experience.position}
            </div>
            <div>
                {experience.description}
            </div>
        </div>
	</div>
  );
}

export default ExperienceComponent;